<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('invoice_year');
            $table->smallInteger('invoice_month');
            $table->smallInteger('invoice_increment');
            $table->integer('total_without_vat')->default(0);
            $table->integer('total_vat')->default(0);
            $table->integer('total_with_vat')->default(0);  
            $table->boolean('is_paid')->default(false);
            $table->foreignId('customer_id')->constrained('customers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
};
