<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Customer;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CustomersTest extends TestCase
{
    use DatabaseMigrations;
    
        /** @test */
        public function a_user_can_read_all_the_customers()
        {
            $this->actingAs(User::factory()->create());
            $customers = Customer::factory()->create();
            $response = $this->get('/customers/customers');
            $response->assertSee($customers->name);
        }

        /** @test */
        public function a_user_can_read_single_customer()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->create();

            $response = $this->get('/customers/customers/'.$customer->id);
            $response->assertSee($customer->name)
                ->assertSee($customer->vat);
        }

        /** @test */
        public function authenticated_users_can_create_a_new_customer()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->make();
            $this->post('/customers/customers',$customer->toArray());
            $this->assertEquals(1,Customer::all()->count());
        }

        /** @test */
        public function unauthenticated_users_cannot_create_a_new_customer()
        {
            $customer = Customer::factory()->make();
            $this->post('/customers/customers',$customer->toArray())
                ->assertRedirect('/login');
        }

        /** @test */
        public function a_customer_requires_a_name()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->make(['name'=>null]);
            $this->post('/customers/customers',$customer->toArray())
                ->assertSessionHasErrors('name');
        }

        /** @test */
        public function a_customer_requires_a_vat()
        {
            $this->actingAs(User::factory()->create());
            $customer = Customer::factory()->make(['vat'=>null]);
            $this->post('/customers/customers',$customer->toArray())
                ->assertSessionHasErrors('vat');
        }

}