<?php

namespace App\Http\Controllers;

use App\Http\Requests\Location\StoreRequest;
use App\Http\Requests\Location\UpdateRequest;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Location;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LocationController extends Controller
{
    public function index(Request $request): View
    {
        $search = $request->get('search', '');

        $locations = Location::search($search)
            ->latest()
            ->with('customer:id,name')
            ->paginate(10);

        return view('locations.index', compact('locations', 'search'));
    }

    public function create(): View
    {
        $countries = Country::pluck('name', 'id');
        $customers = Customer::pluck('name', 'id');

        return view('locations.create', compact('countries', 'customers'));
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        Location::create($request->validated());

        return redirect()
            ->route('locations.index')
            ->withSuccess('Location has been created successfully.');
    }

    public function show(Location $location): View
    {
        return view('locations.show', compact('location'));
    }

    public function edit(Location $location): View
    {
        $countries = Country::pluck('name', 'id');
        $customers = Customer::pluck('name', 'id');

        return view('locations.edit', compact('location', 'countries', 'customers'));
    }

    public function update(Location $location, UpdateRequest $request): RedirectResponse
    {
        $location->update($request->validated());

        return redirect()
            ->route('locations.index')
            ->withSuccess('Location Has Been updated successfully');
    }

    public function destroy(Location $location): RedirectResponse
    {
        $location->delete();

        return redirect()
            ->route('locations.index')
            ->withSuccess('Location has been deleted successfully');
    }
}
