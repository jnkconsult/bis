<?php

namespace App\Http\Controllers;

use App\Http\Requests\Invoice\StoreRequest;
use App\Http\Requests\Invoice\UpdateRequest;
use App\Models\Customer;
use App\Models\Invoice;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class InvoiceController extends Controller
{
    public function index(Request $request): View
    {
        $search = $request->get('search', '');

        $invoices = Invoice::search($search)
            ->latest()
            ->with('customer')
            ->withCount('timesheets')
            ->paginate(10);

        return view('invoices.index', compact('invoices', 'search'));
    }

    public function create(): View
    {
        $customers = Customer::pluck('name', 'id');

        return view('invoices.create', compact('customers'));
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        Invoice::create($request->validated());

        return redirect()
            ->route('invoices.index')
            ->withSuccess('Invoice has been created successfully.');
    }

    public function show(Invoice $invoice): View
    {
        return view('invoices.show', compact('invoice'));
    }

    public function edit(Invoice $invoice): View
    {
        $customers = Customer::pluck('name', 'id');

        return view('invoices.edit', compact('customers', 'invoice'));
    }

    public function update(Invoice $invoice, UpdateRequest $request): RedirectResponse
    {
        $invoice->update($request->validated());

        return redirect()
            ->route('invoices.index')
            ->withSuccess('Invoice Has Been updated successfully');
    }

    public function destroy(Invoice $invoice): RedirectResponse
    {
        $invoice->delete();

        return redirect()
            ->route('invoices.index')
            ->withSuccess('Invoice has been deleted successfully');
    }
}
