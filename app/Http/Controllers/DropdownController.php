<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DropdownController extends Controller
{
    public function fetchState(Request $request): JsonResponse
    {
        $data = [];
        $data['states'] = State::where('country_id', $request->country_id)->get(['name', 'id']);
        return response()->json($data);
    }

    public function fetchCity(Request $request): JsonResponse
    {
        $data = [];
        $data['cities'] = City::where('state_id', $request->state_id)->get(['name', 'id']);
        return response()->json($data);
    }
}
