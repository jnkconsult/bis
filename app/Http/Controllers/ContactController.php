<?php

namespace App\Http\Controllers;

use App\Http\Requests\Contact\StoreRequest;
use App\Http\Requests\Contact\UpdateRequest;
use App\Models\Contact;
use App\Models\Customer;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ContactController extends Controller
{
    public function index(Request $request): View
    {
        $search = $request->get('search', '');

        $contacts = Contact::search($search)
            ->latest()
            ->with('customer:id,name')
            ->paginate(10);

        return view('contacts.index', compact('contacts', 'search'));
    }

    public function create(): View
    {
        $customers = Customer::pluck('name', 'id');

        return view('contacts.create', compact('customers'));
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        Contact::create($request->validated());

        return redirect()
            ->route('contacts.index')
            ->withSuccess('Contact has been created successfully.');
    }

    public function show(Contact $contact): View
    {
        return view('contacts.show', compact('contact'));
    }

    public function edit(Contact $contact): View
    {
        $customers = Customer::pluck('name', 'id');

        return view('contacts.edit', compact('contact', 'customers'));
    }

    public function update(Contact $contact, UpdateRequest $request): RedirectResponse
    {
        $contact->update($request->validated());

        return redirect()
            ->route('contacts.index')
            ->withSuccess('Contact Has Been updated successfully');
    }

    public function destroy(Contact $contact): RedirectResponse
    {
        $contact->delete();

        return redirect()
            ->route('contacts.index')
            ->withSuccess('Contact has been deleted successfully');
    }
}
