<?php

namespace App\Http\Requests\Timesheet;

use App\Enums\PriceTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class StoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'project_id' => 'required|exists:projects,id',
            'description' => 'required',
            'price_type' => ['required', new Enum(PriceTypeEnum::class)],
            'started_at' => 'required|date',
            'ended_at' => 'sometimes',
        ];
    }
}
