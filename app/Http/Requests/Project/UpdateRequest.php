<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'customer_id' => 'required|exists:customers,id',
            'name' => 'required',
            'price_hour' => 'sometimes',
            'price_day' => 'sometimes',
            'price_fix' => 'sometimes',
            'started_at' => 'sometimes|date',
            'ended_at' => 'sometimes',
        ];
    }
}
