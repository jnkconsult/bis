<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use App\Services\Invoice as ServicesInvoice;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Invoice extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'invoice_year',
        'invoice_month',
        'invoice_increment',
        'total_without_vat',
        'total_vat',
        'total_with_vat',
        'is_paid',
        'customer_id',
    ];

    protected $searchableFields = ['*'];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function timesheets(): HasMany
    {
        return $this->hasMany(Timesheet::class);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $invoiceService = new ServicesInvoice();
            $customer = Customer::find($item->customer_id);
            $timesheets = $invoiceService->getTimesheets($customer, $item->invoice_year, $item->invoice_month);
            if ($timesheets) {
                $item->total_without_vat = $invoiceService->calculateTotalInvoice($timesheets);
                $item->total_vat = $invoiceService->calculateTotalVat($item->total_without_vat);
                $item->total_with_vat = $item->total_without_vat + $item->total_vat;
            }

            return $item;
        });

        static::created(function ($item) {
            $invoiceService = new ServicesInvoice();
            $customer = Customer::find($item->customer_id);
            $timesheets = $invoiceService->getTimesheets($customer, $item->invoice_year, $item->invoice_month);
            if ($timesheets) {
                $invoiceService->linkTimesheets($timesheets, $item);
            }
        });
    }
}
