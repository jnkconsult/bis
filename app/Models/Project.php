<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = [
        'name',
        'description',
        'price_hour',
        'price_day',
        'price_fix',
        'started_at',
        'ended_at',
        'customer_id',
    ];

    protected $searchableFields = ['*'];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function timesheets(): HasMany
    {
        return $this->hasMany(Timesheet::class);
    }

    public function setPriceHourAttribute($price): void
    {
        $this->attributes['price_hour'] = $price * 100;
    }

    public function setPriceDayAttribute($price): void
    {
        $this->attributes['price_day'] = $price * 100;
    }

    public function setPriceFixAttribute($price): void
    {
        $this->attributes['price_fix'] = $price * 100;
    }
}
