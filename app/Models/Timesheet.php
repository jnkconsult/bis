<?php

namespace App\Models;

use App\Enums\PriceTypeEnum;
use App\Models\Scopes\Searchable;
use App\Services\Timesheet as ServicesTimesheet;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Timesheet extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['description', 'price_type', 'started_at', 'ended_at', 'project_id', 'invoice_id'];

    protected $casts = ['price_type' => PriceTypeEnum::class, 'started_at' => 'datetime', 'ended_at' => 'datetime'];

    protected $searchableFields = ['*'];

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    public function getPriceAttribute(): int
    {
        $timesheetService = new ServicesTimesheet();
        return $timesheetService->calculateTimeSheetPrice($this) * 100;
    }
}
