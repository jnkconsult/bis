<?php

namespace App\Services;

use App\Models\Customer;
use App\Models\Invoice as ModelsInvoice;
use App\Models\Timesheet;
use Illuminate\Support\Collection;

class Invoice
{
    public function getTimesheets(Customer $customer, int $invoice_year, int $invoice_month): Collection
    {
        return Timesheet::whereHas('project', function ($query) use ($customer) {
            return $query->where('customer_id', '=', $customer->id);
        })
            ->where('invoice_id', null)
            ->whereYear('ended_at', $invoice_year)
            ->whereMonth('ended_at', $invoice_month)
            ->get();
    }

    public function calculateTotalInvoice(Collection $timesheets): int
    {
        $totalInvoice = 0;
        foreach ($timesheets as $timesheet) {
            $totalInvoice += $timesheet->price;
        }

        return $totalInvoice;
    }

    public function calculateTotalVat(int $total_without_vat): int
    {
        return $total_without_vat * 0.21;
    }

    public function linkTimesheets(Collection $timesheets, ModelsInvoice $invoice)
    {
        foreach ($timesheets as $timesheet) {
            $timesheet->invoice_id = $invoice->id;
            $timesheet->save();
        }
    }
}
