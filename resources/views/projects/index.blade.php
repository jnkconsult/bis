<x-app-layout>
    <x-slot name="header">
        {{ __('Project list') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <div class="mb-5 mt-4">
                    <div class="flex flex-wrap justify-between">
                        <div class="md:w-1/2">
                            <x-table-search :search="$search"/>
                        </div>
                        <div class="md:w-1/2 text-right">
                            <x-table-create model="projects" />
                        </div>
                    </div>
                    <x-table-error />
                </div>
                <x-table>
                    <x-slot name="header">
                        <x-table-column>{{ __('Customer') }}</x-table-column>
                        <x-table-column>{{ __('Name') }}</x-table-column>
                        <x-table-column>{{ __('Price hour') }}</x-table-column>
                        <x-table-column>{{ __('Price day') }}</x-table-column>
                        <x-table-column>{{ __('Price fix') }}</x-table-column>
                        <x-table-column>{{ __('Started at') }}</x-table-column>
                        <x-table-column>{{ __('Ended at') }}</x-table-column>
                        <x-table-column>{{ __('Timesheets') }}</x-table-column>
                        <x-table-column>{{ __('Action') }}</x-table-column>
                    </x-slot>
                    @forelse($projects as $row)
                        <tr>
                            <x-table-column><a href="{{ route('customers.show',$row->customer) }}" class="hover:underline">{{ $row->customer->name }}</a></x-table-column>
                            <x-table-column>{{ $row->name }}</x-table-column>
                            <x-table-column><x-format-amount :amount="$row->price_hour" currency="eur" locale="fr_BE" /></x-table-column>
                            <x-table-column><x-format-amount :amount="$row->price_day" currency="eur" locale="fr_BE" /></x-table-column>
                            <x-table-column><x-format-amount :amount="$row->price_fix" currency="eur" locale="fr_BE" /></x-table-column>
                            <x-table-column>{{ $row->started_at ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->ended_at ?? '-' }}</x-table-column>
                            <x-table-column><a href="{{ route('timesheets.index',['search'=>$row->id]) }}"><span class="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-full dark:bg-green-200 dark:text-green-900">{{ $row->timesheets_count }}</span></a></x-table-column>
                            <x-table-column>
                                <x-table-action model="projects" :row="$row"/>
                            </x-table-column>
                        </tr>
                    @empty
                        <tr>
                            <td class="px-4 py-2 border text-red-500" colspan="9">{{ __('No project found.') }}</td>
                        </tr>
                    @endforelse
                    <x-slot name="pagination">
                        {{ $projects->links() }}    
                    </x-slot>
                </x-table>
            </div>
        </div>
    </div>
</x-app-layout>