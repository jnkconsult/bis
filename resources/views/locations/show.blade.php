<x-app-layout>
    <x-slot name="header">
        {{ __('Show Location') }}
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <a href="{{ route('locations.index') }}"
                    class="inline-flex items-center px-4 py-2 mb-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md hover:bg-green-500 active:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-gray disabled:opacity-25">
                    <- Go back
                </a>
                <table class="w-full table-fixed">
                    <tbody>
                        <tr>
                            <td class="px-4 py-2 font-bold">Company</td>
                            <td>{{ $location->customer->name }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Street</td>
                            <td>{{ $location->street }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Street nr</td>
                            <td>{{ $location->street_number }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Street box</td>
                            <td>{{ $location->street_box }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Country</td>
                            <td>{{ $location->country->name }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">State</td>
                            <td>{{ $location->state->name }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">City</td>
                            <td>{{ $location->city->name }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Zip</td>
                            <td>{{ $location->zip }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Created on</td>
                            <td>{{ date_format($location->created_at, 'jS F Y g:i A') }}</td>
                        </tr>
                        <tr>
                            <td class="px-4 py-2 font-bold">Last updated</td>
                            <td>{{ date_format($location->updated_at, 'jS F Y g:i A') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>