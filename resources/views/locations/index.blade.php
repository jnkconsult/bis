<x-app-layout>
    <x-slot name="header">
        {{ __('Location list') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <div class="mb-5 mt-4">
                    <div class="flex flex-wrap justify-between">
                        <div class="md:w-1/2">
                            <x-table-search :search="$search"/>
                        </div>
                        <div class="md:w-1/2 text-right">
                            <x-table-create model="locations" />
                        </div>
                    </div>
                    <x-table-error />
                </div>
                <x-table>
                    <x-slot name="header">
                        <x-table-column>{{ __('Customer') }}</x-table-column>
                        <x-table-column>{{ __('Street') }}</x-table-column>
                        <x-table-column>{{ __('Street nr') }}</x-table-column>
                        <x-table-column>{{ __('street box') }}</x-table-column>
                        <x-table-column>{{ __('zip') }}</x-table-column>
                        <x-table-column>{{ __('city') }}</x-table-column>
                        <x-table-column>{{ __('country') }}</x-table-column>
                        <x-table-column>{{ __('Action') }}</x-table-column>
                    </x-slot>   
                    @forelse($locations as $row)
                        <tr>
                            <x-table-column><a href="{{ route('customers.show',$row->customer) }}" class="hover:underline">{{ optional($row->customer)->name }}</a></x-table-column>
                            <x-table-column>{{ $row->street ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->street_number ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->street_box ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->zip ?? '-' }}</x-table-column>
                            <x-table-column>{{ optional($row->city)->name }}</x-table-column>
                            <x-table-column>{{ optional($row->country)->name }}</x-table-column>
                            <x-table-column>
                                <x-table-action model="locations" :row="$row"/>
                            </x-table-column>
                        </tr>
                    @empty   
                        <tr>
                            <td class="px-4 py-2 border text-red-500" colspan="8">{{ __('No location found.') }}</td>
                        </tr> 
                    @endforelse
                    <x-slot name="pagination">
                        {{ $locations->links() }}    
                    </x-slot>
                </x-table>
            </div>
        </div>
    </div>
</x-app-layout>