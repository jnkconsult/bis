<form action="{{ route($model.'.destroy', $row->id) }}" method="POST">
    <a href="{{ route($model.'.show', $row->id) }}" class="px-1 text-xs text-blue-600 dark:text-blue-500 hover:underline">
        Show
    </a>
    <a href="{{ route($model.'.edit', $row->id) }}" class="px-1 text-xs text-blue-600 dark:text-blue-500 hover:underline">
        Edit
    </a>
    @csrf
    @method('DELETE')
    @if (empty($condition))
        <button type="submit" title="delete" class="px-1 text-xs text-red-600 dark:text-red-500 hover:underline">
            Delete
        </button>
    @else
        @if ($condition === 0)
            <button type="submit" title="delete" class="px-1 text-xs text-red-600 dark:text-red-500 hover:underline">
                Delete
            </button>
        @endif
    @endif
</form>