<table class="w-full table-fixed">
    <thead>
        <tr class="bg-gray-100 font-bold text-center">
            {{ $header }}
        </tr>
    </thead>
    <tbody>
        {{ $slot }}
    </tbody>
</table>
<div class="row py-2">
    <div class="col-md-12">
        {{ $pagination }}
    </div>
</div>