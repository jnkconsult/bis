<form>
    <div class="flex items-center w-full">
        <x-input
            name="search"
            class="block appearance-none w-full py-1 px-2 text-base leading-normal text-gray-800 border border-gray-200 rounded"
            value="{{ $search ?? '' }}"
            placeholder="{{ __('search') }}"
            autocomplete="off"
        ></x-input>

        <div class="ml-1">
            <button
                type="submit"
                class="button button-primary"
            >
                <i class="icon ion-md-search"></i>
            </button>
        </div>
    </div>
</form>