<x-app-layout>
    <x-slot name="header">
        {{ __('Contact list') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <div class="mb-5 mt-4">
                    <div class="flex flex-wrap justify-between">
                        <div class="md:w-1/2">
                            <x-table-search :search="$search"/>
                        </div>
                        <div class="md:w-1/2 text-right">
                            <x-table-create model="contacts" />
                        </div>
                    </div>
                    <x-table-error />
                </div>
                <x-table>
                    <x-slot name="header">
                        <x-table-column>{{ __('Customer') }}</x-table-column>
                        <x-table-column>{{ __('Firstname') }}</x-table-column>
                        <x-table-column>{{ __('Lastname') }}</x-table-column>
                        <x-table-column>{{ __('Email') }}</x-table-column>
                        <x-table-column>{{ __('Phone') }}</x-table-column>
                        <x-table-column>{{ __('Action') }}</x-table-column>
                    </x-slot>
                    @forelse($contacts as $row)
                        <tr>
                            <x-table-column><a href="{{ route('customers.show',$row->customer) }}" class="hover:underline">{{ $row->customer->name }}</a></x-table-column>
                            <x-table-column>{{ $row->firstname ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->lastname ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->email ?? '-' }}</x-table-column>
                            <x-table-column>{{ $row->phone ?? '-' }}</x-table-column>
                            <x-table-column>
                                <x-table-action model="contacts" :row="$row"/>
                            </x-table-column>
                        </tr>
                    @empty 
                        <tr>
                            <td class="px-4 py-2 border text-red-500" colspan="6">{{ __('No contact found.') }}</td>
                        </tr>
                    @endforelse
                    <x-slot name="pagination">
                        {{ $contacts->links() }}    
                    </x-slot>
                </x-table>
            </div>
        </div>
    </div>
</x-app-layout>