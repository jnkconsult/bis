<x-app-layout>
    <x-slot name="header">
        {{ __('Edit Customer') }}
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
                <a href="{{ route('customers.index') }}"
                    class="inline-flex items-center px-4 py-2 mb-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-green-600 border border-transparent rounded-md hover:bg-green-500 active:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-gray disabled:opacity-25">
                    <- Go back
                </a>
                <x-validation-errors class="mb-4" :errors="$errors" />
                <form action="{{ route('customers.update', $customer) }}" method="POST">
                    @csrf
                    @method('PUT')
                <div class="mb-4">
                    <x-label for="name" :value="__('customer.name')" class="required" />

                    <x-input id="name" class="block mt-1 w-full"
                        type="text"
                        name="name"
                        value="{{ old('name', $customer->name) }}"
                        required />
                </div>
                <div class="mb-4">
                    <x-label for="vat" :value="__('customer.vat')" class="required" />

                    <x-input id="vat" class="block mt-1 w-full"
                        type="text"
                        name="vat"
                        value="{{ old('vat', $customer->vat) }}"
                        required />
                </div>
                <div>
                    <x-button>{{ __('customer.submit') }}</x-button>
                </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>